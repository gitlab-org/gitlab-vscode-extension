import * as vscode from 'vscode';
import { runDuoWorkflowCommand, runDuoWorkflowSearchCommand } from './commands';

jest.mock('../../common/security_scans/run_security_scan', () => ({ runSecurityScan: jest.fn() }));
jest.mock('vscode', () => ({
  window: {
    showInputBox: jest.fn(),
    showInformationMessage: jest.fn(),
  },
  commands: {
    executeCommand: jest.fn(),
  },
}));

describe('runDuoWorkflowCommand', () => {
  it('should prompt for workflow goal', async () => {
    await runDuoWorkflowCommand();
    expect(vscode.window.showInputBox).toHaveBeenCalledWith({
      prompt: 'Enter Workflow Goal',
      placeHolder: 'Type your workflow goal here...',
    });
  });

  it('should execute duoWorkflow command with goal when input is provided', async () => {
    const mockGoal = 'Test workflow';
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue(mockGoal);

    await runDuoWorkflowCommand();

    expect(vscode.commands.executeCommand).toHaveBeenCalledWith('gl.webview.duoWorkflow.show', {
      goal: mockGoal,
    });
    expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
      'Running workflow for: Test workflow',
    );
  });

  it('should not execute duoWorkflow command when input is cancelled', async () => {
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue(undefined);

    await runDuoWorkflowCommand();

    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
  });

  it('should handle empty input', async () => {
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue('');

    await runDuoWorkflowCommand();

    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
  });
});

describe('runDuoWorkflowSearchCommand', () => {
  it('should show prompt for workflow goal', async () => {
    await runDuoWorkflowSearchCommand();
    expect(vscode.window.showInputBox).toHaveBeenCalledWith({
      prompt: 'Enter directory path to scan ',
      placeHolder: 'Type the directory path here...',
    });
  });

  it('should not execute duoWorkflow command when input is cancelled', async () => {
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue(undefined);

    await runDuoWorkflowSearchCommand();

    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
  });

  it('should handle empty input', async () => {
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue(undefined);

    await runDuoWorkflowSearchCommand();

    expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
    expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
  });

  it('should execute duoWorkflow command with goal when input is provided', async () => {
    const mockPath = 'src/app/code';
    (vscode.window.showInputBox as jest.Mock).mockResolvedValue(mockPath);

    await runDuoWorkflowSearchCommand();

    expect(vscode.commands.executeCommand).toHaveBeenCalledWith('gl.webview.duoWorkflow.show', {
      goal: mockPath,
      type: 'search_and_replace',
    });

    expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
      'Running workflow to search and replace in src/app/code',
    );
  });
});
