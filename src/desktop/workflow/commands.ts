import * as vscode from 'vscode';
import { USER_COMMANDS } from '../command_names';
import { doNotAwait } from '../../common/utils/do_not_await';

enum WorkflowType {
  SEARCH_AND_REPLACE = 'search_and_replace',
  SOFTWARE_DEVELOPMENT = 'software_development',
}

export const runDuoWorkflowCommand = async () => {
  const goal = await vscode.window.showInputBox({
    prompt: 'Enter Workflow Goal',
    placeHolder: 'Type your workflow goal here...',
  });

  if (goal) {
    doNotAwait(vscode.window.showInformationMessage(`Running workflow for: ${goal}`));
    await vscode.commands.executeCommand(USER_COMMANDS.SHOW_DUO_WORKFLOW_WEBVIEW, { goal });
  }
};

export const runDuoWorkflowSearchCommand = async () => {
  const path = await vscode.window.showInputBox({
    prompt: 'Enter directory path to scan ',
    placeHolder: 'Type the directory path here...',
  });

  if (path) {
    doNotAwait(
      vscode.window.showInformationMessage(`Running workflow to search and replace in ${path}`),
    );
    await vscode.commands.executeCommand(USER_COMMANDS.SHOW_DUO_WORKFLOW_WEBVIEW, {
      goal: path,
      type: WorkflowType.SEARCH_AND_REPLACE,
    });
  }
};
