import * as vscode from 'vscode';
import { NodeModule, LanguageClient, TransportKind } from 'vscode-languageclient/node';
import {
  LANGUAGE_SERVER_ID,
  LANGUAGE_SERVER_NAME,
  LanguageClientFactory,
} from '../../common/language_server/client_factory';
import { getExtensionConfiguration } from '../../common/utils/extension_configuration';

function proxyEnv():
  | {
      http_proxy?: string | undefined;
      HTTPS_PROXY?: string | undefined;
      NO_PROXY?: string | undefined;
    }
  | undefined {
  const httpProxy: string =
    vscode.workspace.getConfiguration('http').get('proxy') || process.env.http_proxy || '';
  const httpsProxy: string =
    vscode.workspace.getConfiguration('http').get('proxy') || process.env.HTTPS_PROXY || '';
  const noProxy: string[] =
    vscode.workspace.getConfiguration('http').get('noProxy') ||
    process.env.NO_PROXY?.split(',') ||
    [];
  const env = {
    ...(httpProxy ? { http_proxy: httpProxy } : {}),
    ...(httpsProxy ? { HTTPS_PROXY: httpsProxy } : {}),
    ...(noProxy.length > 0 ? { NO_PROXY: noProxy.join(',') } : {}),
  };
  if (env.http_proxy === undefined && env.HTTPS_PROXY === undefined && env.NO_PROXY === undefined) {
    return undefined;
  }
  return env;
}

export const desktopLanguageClientFactory: LanguageClientFactory = {
  createLanguageClient(context, clientOptions) {
    const exec: NodeModule = {
      module: context.asAbsolutePath('./assets/language-server/node/main-bundle.js'),
      transport: TransportKind.stdio,
    };

    const runArgs = getExtensionConfiguration().debug ? ['--use-source-maps'] : []; // this initializes source maps for stack trace

    const env = proxyEnv();
    const options = env?.HTTPS_PROXY || env?.http_proxy ? { env } : undefined;

    return new LanguageClient(
      LANGUAGE_SERVER_ID,
      LANGUAGE_SERVER_NAME,
      {
        debug: {
          ...exec,
          args: runArgs,
          options: {
            ...options,
            execArgv: ['--nolazy', '--inspect=6010'],
          },
        },
        run: {
          ...exec,
          args: runArgs,
          options,
        },
      },
      clientOptions,
    );
  },
};
