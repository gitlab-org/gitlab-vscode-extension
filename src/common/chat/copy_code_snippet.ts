import * as vscode from 'vscode';

export const copyCodeSnippet = async (snippet: string) => {
  try {
    await vscode.env.clipboard.writeText(snippet);
    await vscode.window.showInformationMessage('Snippet copied to clipboard.');
  } catch (error) {
    await vscode.window.showErrorMessage(
      `Error copying snippet: ${error instanceof Error ? error.message : 'Unknown error'}`,
    );
  }
};
