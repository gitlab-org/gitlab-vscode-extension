import * as vscode from 'vscode';
import { FeatureState } from '@gitlab-org/gitlab-lsp';
import { diffEmitter } from '../utils/diff_emitter';

export class FeatureStateManager {
  #eventEmitter = diffEmitter(new vscode.EventEmitter<FeatureState[]>());

  #states: FeatureState[] = [];

  onChange(
    listener: (e: FeatureState[]) => void,
    thisArgs?: unknown,
    disposables?: vscode.Disposable[],
  ): vscode.Disposable {
    // Emit the current state immediately when this listener is added
    listener(this.state);

    // Subscribe the listener to future state changes
    const disposable = this.#eventEmitter.event(listener, thisArgs, disposables);

    return new vscode.Disposable(() => disposable.dispose());
  }

  setStates = (states: FeatureState[]) => {
    this.#states = states;
    this.#eventEmitter.fire(this.state);
  };

  get state(): FeatureState[] {
    return this.#states;
  }
}
